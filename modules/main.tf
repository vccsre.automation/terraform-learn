terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      #version = "~> 3.21" # Optional but recommended in production
    }
  }
}

provider "aws" {
  profile = "default" # AWS Credentials Profile configured on your local desktop terminal  $HOME/.aws/credentials
  region  = "us-east-1"
}


#this code is for vpc
resource "aws_vpc" "demo-vpc" {
  cidr_block = var.vpc_cidr_block
  tags = {
      Name: "${var.env_prefix}-vpc"
  }
}


module "demo-subnet" {
    source = "./module01/subnet"
    subnet_cidr_block = var.subnet_cidr_block
    avail_zone = var.avail_zone
    env_prefix = var.env_prefix
    vpc_id = aws_vpc.demo-vpc.id
}

#this code is for subnet
resource "aws_subnet" "demo-subnet" {
    vpc_id = aws_vpc.demo-vpc.id
    cidr_block = var.subnet_cidr_block
    availability_zone = var.avail_zone
    tags = {
        Name: "${var.env_prefix}-subnet-1 "
    }
  
}

#this code is for route table
resource "aws_route_table" "demo-route" {
    vpc_id = aws_vpc.demo-vpc.id 
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.gateway-demo.id
    } 
    tags = {
        Name: "${var.env_prefix}-rtb"
    }
}

resource "aws_internet_gateway" "gateway-demo" {
    vpc_id = aws_vpc.demo-vpc.id
    tags = {
        Name: "${var.env_prefix}-igw"
    }
}


resource "aws_route_table_association" "demo-subnet-association" {
    subnet_id = aws_subnet.demo-subnet.id  
    route_table_id = aws_route_table.demo-route.id
}

#port open in securty group
resource "aws_security_group" "demo-sg" {
    name = "demo-sg"
    vpc_id = aws_vpc.demo-vpc.id
     ingress {
         from_port = 22
         to_port = 22
         protocol = "tcp"
         cidr_blocks = ["0.0.0.0/0"]
     } 

     ingress {
         from_port = 8080
         to_port = 8080
         protocol = "tcp"
         cidr_blocks = ["0.0.0.0/0"]
     }

     egress {
         from_port = 0
         to_port = 0
         protocol = "-1"
         cidr_blocks = ["0.0.0.0/0"]
         prefix_list_ids = []
     }
     tags = {
        Name: "${var.env_prefix}-sg"
    }

}




data "aws_ami" "latest-aws-linux" {
    most_recent = true
    owners = ["amazon"]
    filter {
        name = "name"
        values = ["amzn2-ami-hvm-*-x86_64-gp2"]
    }

    # filter {
    #     name = "Virtualization-type"
    #     values = ["hvm"]
    # }
}
#creating Aws Instance

resource "aws_instance" "demo-ec2" {
    ami = data.aws_ami.latest-aws-linux.id
    instance_type = var.instance_type
    #subnet_id =  aws_subnet.demo-subnet.id
    subnet_id = module.demo-subnet.subnet
    vpc_security_group_ids = [aws_security_group.demo-sg.id]
    availability_zone = var.avail_zone
    associate_public_ip_address = true
    #key_name = "learning26"
    key_name = aws_key_pair.demo-key.key_name
    #run script on server at boot time
    user_data = file("entry-script.sh")

    tags = {
        Name =  "${var.env_prefix}-server"
    }
}



#create public key 

resource "aws_key_pair" "demo-key" {
    key_name = "server-key"
    public_key = "${file(var.public_key_location)}"  
}

output "aws-ami-id" { 
    value = data.aws_ami.latest-aws-linux.image_id
}

output "vpc_info" {
    value = aws_vpc.demo-vpc.arn
}

output "aws-subnet" {
    value = aws_subnet.demo-subnet.arn
}

output "public_ip" {
    value = aws_instance.demo-ec2.public_ip
  
}



