variable username {
  type        = list
  description = "creating user"
  default     = ["eks", "devops"]
}


variable clustername {
  type        = string
  description = "eks clustername"
  default = "Mumbai_prod"
}

variable eksnodegroup {
  type        = string
  description = "eks node group name"
  default = "Mumbai_prod_worker"
}
