terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      #version = "~> 3.21" # Optional but recommended in production
    }
  }
}

# Provider Block
provider "aws" {
  profile = "default" # AWS Credentials Profile configured on your local desktop terminal  $HOME/.aws/credentials
  region  = "us-east-1"
}


resource "aws_vpc"  "development_vpc" {
    cidr_block = "10.0.0.0/16"
    tags = {
        Name: "development"
    }
}

resource "aws_subnet" "dev-subnet-1" {
    vpc_id = aws_vpc.development_vpc.id 
    cidr_block = "10.0.10.0/24"
    availability_zone = "us-east-1a"
    tags = {
        Name: "subnet-1-dev"
    }
}

variable "ec2_instance" {
    description = "ec2 instance type"
    # default = "t2.micro"
}

variable "ec2_hostname" {
    description = "ec2 instance name"
}

# Resource Block
resource "aws_instance" "ec2demo" {
  ami = "ami-04d29b6f966df1537" # Amazon Linux in us-east-1, update as per your region
  instance_type = var.ec2_instance
  tags = {
      Name: var.ec2_hostname
  }
}
 



output "aws-vpc-value" {
    value =  aws_vpc.development_vpc.id
}


output "ec2" {
    value = aws_instance.ec2demo.public_ip
    description = "The private IP address of the main server instance."
    sensitive   = true
}
