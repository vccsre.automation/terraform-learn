output "subnet" {
    value = aws_subnet.demo-subnet  
}

output "aws-subnet" {
    value = aws_subnet.demo-subnet.arn
}