variable "subnet_cidr_block" {
    description = "subnet cidr block"
}

variable "avail_zone" {
    description = "availability zone"
}

variable env_prefix {
    description = "select the env"
}


variable vpc_id {
    description = "this is for vpc"
} 