
output "aws-ami-id" { 
    value = data.aws_ami.latest-aws-linux.image_id
}

output "public_ip" {
    value = aws_instance.demo-ec2.public_ip 
}
