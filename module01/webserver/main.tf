resource "aws_security_group" "demo-sg" {
    name = "demo-sg"
    vpc_id = var.vpc_id
     ingress {
         from_port = 22
         to_port = 22
         protocol = "tcp"
         cidr_blocks = ["0.0.0.0/0"]
     } 

     ingress {
         from_port = 8080
         to_port = 8080
         protocol = "tcp"
         cidr_blocks = ["0.0.0.0/0"]
     }

     egress {
         from_port = 0
         to_port = 0
         protocol = "-1"
         cidr_blocks = ["0.0.0.0/0"]
         prefix_list_ids = []
     }
     tags = {
        Name: "${var.env_prefix}-sg"
    }

}




data "aws_ami" "latest-aws-linux" {
    most_recent = true
    owners = ["amazon"]
    filter {
        name = "name"
        #values = ["amzn2-ami-hvm-*-x86_64-gp2"]
        values = [var.image_name]
    }
}


resource "aws_key_pair" "demo-key" {
    key_name = "server-key"
    public_key = "${file(var.public_key_location)}"  
}


#creating Aws Instance

resource "aws_instance" "demo-ec2" {
    ami = data.aws_ami.latest-aws-linux.id
    instance_type = var.instance_type
    #subnet_id =  aws_subnet.demo-subnet.id
    #subnet_id = module.demo-subnet.subnet.id
    subnet_id = var.subnet_id
    vpc_security_group_ids = [aws_security_group.demo-sg.id]
    #vpc_security_group_ids = [var.default_sg_id]
    availability_zone = var.avail_zone
    associate_public_ip_address = true
    #key_name = "learning26"
    key_name = aws_key_pair.demo-key.key_name
    #run script on server at boot time
    user_data = file("entry-script.sh")

    tags = {
        Name =  "${var.env_prefix}-server"
    }
}
