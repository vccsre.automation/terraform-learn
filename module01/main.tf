terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      #version = "~> 3.21" # Optional but recommended in production
    }
  }
}

provider "aws" {
  profile = "default" # AWS Credentials Profile configured on your local desktop terminal  $HOME/.aws/credentials
  region  = "us-east-1"
}


#this code is for vpc
resource "aws_vpc" "demo-vpc" {
  cidr_block = var.vpc_cidr_block
  tags = {
      Name: "${var.env_prefix}-vpc"
  }
}


module "demo-subnet" {
    source = "./subnet"
    subnet_cidr_block = var.subnet_cidr_block
    avail_zone = var.avail_zone
    env_prefix = var.env_prefix
    vpc_id = aws_vpc.demo-vpc.id
}


module "demo-server" {
    source = "./webserver"
    vpc_id = aws_vpc.demo-vpc.id
    env_prefix = var.env_prefix
    image_name = var.image_name
    public_key_location = var.public_key_location
    instance_type = var.instance_type
    subnet_id = module.demo-subnet.subnet.id
    avail_zone = var.avail_zone
}
#this code is for subnet
# resource "aws_subnet" "demo-subnet" {
#     vpc_id = aws_vpc.demo-vpc.id
#     cidr_block = var.subnet_cidr_block
#     availability_zone = var.avail_zone
#     tags = {
#         Name: "${var.env_prefix}-subnet-1 "
#     }
  
# }

# #this code is for route table
# resource "aws_route_table" "demo-route" {
#     vpc_id = aws_vpc.demo-vpc.id 
#     route {
#         cidr_block = "0.0.0.0/0"
#         gateway_id = aws_internet_gateway.gateway-demo.id
#     } 
#     tags = {
#         Name: "${var.env_prefix}-rtb"
#     }
# }

# resource "aws_internet_gateway" "gateway-demo" {
#     vpc_id = aws_vpc.demo-vpc.id
#     tags = {
#         Name: "${var.env_prefix}-igw"
#     }
# }


# resource "aws_route_table_association" "demo-subnet-association" {
#     subnet_id = aws_subnet.demo-subnet.id  
#     route_table_id = aws_route_table.demo-route.id
# }

#port open in securty group



#create public key 

# resource "aws_key_pair" "demo-key" {
#     key_name = "server-key"
#     public_key = "${file(var.public_key_location)}"  
# }


output "vpc_info" {
    value = aws_vpc.demo-vpc.arn
}






