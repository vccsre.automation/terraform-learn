variable "vpc_cidr_block" {
    description = "vpc cidr block"
}

variable "subnet_cidr_block" {
    description = "subnet cidr block"
}

variable "avail_zone" {
    description = "availability zone"
}

variable env_prefix {
    description = "select the env"
}

variable instance_type {
    description = "instance type of machine"
}
variable "public_key_location" {
    description = "public_key_location"
}
variable "private_key_location" {
    description = "private_key_location"
}
variable "image_name" {
    description = "image name here"
}
